import 'package:flutter/material.dart';
import 'package:telecom_fe_flutter/src/utils/weather_icons.dart';

class Weather {
  int id;
  int time;
  String iconCode;
  String cityName;
  double temperature;
  double maxTemperature;
  double minTemperature;

  Weather(
      {this.id,
      this.time,
      this.iconCode,
      this.cityName,
      this.temperature,
      this.maxTemperature,
      this.minTemperature});

  static Weather fromJson(Map<String, dynamic> json) {
    final weather = json['weather'][0];
    return Weather(
      id: weather['id'],
      time: json['dt'],
      iconCode: weather['icon'],
      cityName: json['name'],
      temperature: json['main']['temp'] is int ? (json['main']['temp'] as int).toDouble() : json['main']['temp'],
      maxTemperature: json['main']['temp_max'] is int ? (json['main']['temp_max'] as int).toDouble() : json['main']['temp_max'],
      minTemperature: json['main']['temp_min'] is int ? (json['main']['temp_min'] as int).toDouble() : json['main']['temp_min'],
    );
  }

  static List<Weather> fromForecastJson(Map<String, dynamic> json) {
    final weathers = List<Weather>();
    for (final item in json['list']) {
      weathers.add(Weather(
          time: item['dt'],
          maxTemperature: item['temp']['max'] is int ? (item['temp']['max'] as int).toDouble() : item['temp']['max'],
          minTemperature: item['temp']['min'] is int ? (item['temp']['min'] as int).toDouble() : item['temp']['min'],
          iconCode: item['weather'][0]['icon']
      ));
    }
    return weathers;
  }

  IconData getIconData(){
    switch(this.iconCode){
      case '01d': return WeatherIcons.clear_day;
      case '01n': return WeatherIcons.clear_night;
      case '02d': return WeatherIcons.few_clouds_day;
      case '02n': return WeatherIcons.few_clouds_day;
      case '03d':
      case '04d':
        return WeatherIcons.clouds_day;
      case '03n':
      case '04n':
        return WeatherIcons.clear_night;
      case '09d': return WeatherIcons.shower_rain_day;
      case '09n': return WeatherIcons.shower_rain_night;
      case '10d': return WeatherIcons.rain_day;
      case '10n': return WeatherIcons.rain_night;
      case '11d': return WeatherIcons.thunder_storm_day;
      case '11n': return WeatherIcons.thunder_storm_night;
      case '13d': return WeatherIcons.snow_day;
      case '13n': return WeatherIcons.snow_night;
      case '50d': return WeatherIcons.mist_day;
      case '50n': return WeatherIcons.mist_night;
      default: return WeatherIcons.clear_day;
    }
  }
}