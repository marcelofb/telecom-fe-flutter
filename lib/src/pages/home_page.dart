import 'package:flutter/material.dart';
import 'package:telecom_fe_flutter/src/blocs/provider.dart';
import 'package:telecom_fe_flutter/src/models/weather.dart';
import 'package:simple_moment/simple_moment.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _selectedOption;
  static const Map<String, dynamic> _locations = {
    'Select city': null,
    'Current location': 'current',
    'Córdoba': 'cordoba,ar',
    'Mendoza': 'mendoza,ar',
    'Posadas': 'posadas,ar',
    'Rawson': 'rawson,ar',
    'Salta': 'salta,ar'
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(top: 20),
          height: double.infinity,
          width: double.infinity,
          child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: Column(
              children: <Widget>[
                _createDropdown(),
                Stack(
                  children: <Widget>[
                    _showData(),
                    StreamBuilder<bool>(
                      stream: Provider.of(context).isLoadingStream,
                      builder: (_, snapshot) =>
                        snapshot.hasData && snapshot.data
                          ? Center(child: CircularProgressIndicator(),)
                          : Container()
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _createDropdown() {
    return DropdownButton(
      value: _selectedOption,
      items: _getDropdownOptions(),
      onChanged: (opt) {
        setState(() {
          _selectedOption = opt;
        });
        Provider.of(context).getData(opt);
      }
    );
  }

  List<DropdownMenuItem<String>> _getDropdownOptions() {
    List<DropdownMenuItem<String>> lista = List();
    _locations.forEach((k, v){
      lista.add(DropdownMenuItem(
        child: Text('$k'),
        value: v,
      ));
    });
    return lista;
  }
  
  _showData() {
    return Column(
      children: <Widget>[
        SizedBox(height: 20,),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            children: <Widget>[
              _showCurrent(),
              SizedBox(height: 20,),
              _showForecast()
            ],
          ),
        )
      ],
    );
  }

  _showCurrent() {
    return StreamBuilder<Weather>(
      stream: Provider.of(context).currentStream,
      builder: (_, snapshot) =>
        snapshot.hasData
          ? _buildCurrent(snapshot.data)
          : Container()
    );
  }

  _buildCurrent(Weather current) {
    final minMaxTempStyle = TextStyle(fontSize: 16, fontWeight: FontWeight.w500);
    return Column(
      children: <Widget>[
        Text(current.cityName.toUpperCase(),
        style: TextStyle(
          fontWeight: FontWeight.w900,
          letterSpacing: 5,
          fontSize: 24),
        ),
        SizedBox(height: 10,),
        Container(
          padding: EdgeInsets.symmetric(vertical: 10),
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              border: Border.all(color: Colors.grey),
            ),
            child: Column(
              children: [
                Icon(
                  current.getIconData(),
                  size: 70,
                ),
                SizedBox(height: 20,),
                Text(
                  '${current.temperature.round()}°',
                  style: TextStyle(
                      fontSize: 100,
                      fontWeight: FontWeight.bold,),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text('Min: ${current.minTemperature.round()}', style: minMaxTempStyle,),
                    Text('Max: ${current.maxTemperature.round()}', style: minMaxTempStyle,),
                  ],
                ),
              ]
            ),
        ),
      ],
    );
  }

  _showForecast() {
    return StreamBuilder<List<Weather>>(
      stream: Provider.of(context).forecastStream,
      builder: (_, snapshot) =>
        snapshot.hasData
          ? _buildForecast(snapshot.data)
          : Container()
    );
  }

  _buildForecast(List<Weather> forecastList) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Forecast',style: TextStyle(
                fontWeight: FontWeight.w700,
                letterSpacing: 5,
                fontSize: 15),),
        SizedBox(height: 10,),
        Container(
          child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: forecastList.length,
            itemBuilder: (BuildContext context, int index) {
              final Weather forecast = forecastList[index];
              final forecastDay = Moment.fromMillisecondsSinceEpoch(forecast.time * 1000).format('EEEE');
              return index != 0
                ? Ink(
                  color: (index.isOdd) ? Colors.grey[100] : Colors.white,
                  child: ListTile(
                    leading: Icon(
                      forecast.getIconData(),
                    ),
                    title: Text('$forecastDay'),
                    trailing: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      spacing: 12,
                      children: <Widget>[
                        Text('${forecast.maxTemperature.round()}', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                        Text('${forecast.minTemperature.round()}', style: TextStyle(fontSize: 16),),
                      ],
                    ),
                  ),
                )
                : Container();
            }
          ),
        ),
      ],
    );
  }
}