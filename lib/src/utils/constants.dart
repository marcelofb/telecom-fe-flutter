class Constants {
  static const String OPEN_WEATHER_MAP = '21607e0b394183778bfee5bb7fc3bc6e';
  static const String BASE_URL = 'http://api.openweathermap.org';
  static const String CURRENT_LOCATION_URL = 'http://ip-api.com/json';
}