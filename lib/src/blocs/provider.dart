import 'package:flutter/widgets.dart';
import 'package:telecom_fe_flutter/src/blocs/weather_bloc.dart';

class Provider extends InheritedWidget {
  static Provider _instancia;

  factory Provider({Key key, Widget child}) {
    if (_instancia == null) {
      _instancia = Provider._internal(key: key, child: child);
    }
    return _instancia;
  }

  Provider._internal({Key key, Widget child})
  : super(key: key, child: child);

  final weatherBloc = WeatherBloc();

  static WeatherBloc of (BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<Provider>().weatherBloc;
  }
  
  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}