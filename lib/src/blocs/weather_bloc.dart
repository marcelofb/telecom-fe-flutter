import 'dart:async';

import 'package:telecom_fe_flutter/src/api/weather_provider.dart';
import 'package:telecom_fe_flutter/src/models/weather.dart';

class WeatherBloc {
  final _weatherProvider = WeatherProvider();
  
  final _currentController = StreamController<Weather>.broadcast();
  final _forecastController = StreamController<List<Weather>>.broadcast();
  final _isLoadingController = StreamController<bool>.broadcast();

  Function(Weather) get currentSink => _currentController.sink.add;
  Stream<Weather> get currentStream => _currentController.stream;

  Function(List<Weather>) get forecastSink => _forecastController.sink.add;
  Stream<List<Weather>> get forecastStream => _forecastController.stream;

  Function(bool) get isLoadingSink => _isLoadingController.sink.add;
  Stream<bool> get isLoadingStream => _isLoadingController.stream;

  getData(String selectedLocation) async {
    isLoadingSink(true);
    var location = selectedLocation;
    if (selectedLocation == 'current') {
      location = await _weatherProvider.getCurrentLocation();
    }
    if (location != null) {
      var futures = <Future>[_weatherProvider.getWeather(location), _weatherProvider.getForecast(location)];
      final res = await Future.wait(futures);
      _currentController.sink.add(res[0]);
      _forecastController.sink.add(res[1]);
    } else {
      _currentController.sink.add(null);
      _forecastController.sink.add(null);
    }
    isLoadingSink(false);
  }

  dispose() {
    _currentController?.close();
    _forecastController?.close();
    _isLoadingController?.close();
  }
}