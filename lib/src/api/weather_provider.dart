import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:telecom_fe_flutter/src/utils/constants.dart';
import 'package:telecom_fe_flutter/src/models/weather.dart';

class WeatherProvider {
  
  Future<String> getCurrentLocation() async {
    final res = await http.get(Constants.CURRENT_LOCATION_URL);
    if (res.statusCode != 200) {
      throw Exception('getCurrentLocation service error');
    }
    final locationJson = json.decode(res.body);
    return '${locationJson['city']},${locationJson['countryCode']}';
  }

  Future<Weather> getWeather(String location) async {
    final url = '${Constants.BASE_URL}/data/2.5/weather?q=$location&appid=${Constants.OPEN_WEATHER_MAP}&units=metric';
    final res = await http.get(url);
    if (res.statusCode != 200) {
      throw Exception('getWeather service error');
    }
    final weatherJson = json.decode(res.body);
    return Weather.fromJson(weatherJson);
  }

  Future<List<Weather>> getForecast(String location) async {
    final url = '${Constants.BASE_URL}/data/2.5/forecast/daily?q=$location&cnt=6&appid=${Constants.OPEN_WEATHER_MAP}&units=metric';
    final res = await http.get(url);
    if (res.statusCode != 200) {
      throw Exception('getForecast service error');
    }
    final forecastJson = json.decode(res.body);
    List<Weather> weathers = Weather.fromForecastJson(forecastJson);
    return weathers;
  }
}